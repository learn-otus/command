import { VELOCITY_KEY, POSITION_KEY } from '../src/Adapters/MovebleAdapter';
import { Tank } from '../src/UObjects/Tank';
import { POSITION_ANGLE_KEY, VELOCITY_ROTATE_KEY } from '../src/Adapters/RotatebleAdepter';
import { FACTOR_OF_SPEED_KEY, ChangeAngleVelosityAdaptor } from '../src/Adapters/ChangeAngleVelosityAdaptor';
import { ChangeAngleVelosityCommand } from '../src/Commands/ChangeAngleVelosityCommand';
describe('Проверка на изменение угла скорости', () => {
  const obj = new Tank({
    [POSITION_KEY]: { x: 10, y: 20}, 
    [VELOCITY_KEY]: { x: 1, y: 0},
    [POSITION_ANGLE_KEY]: 45,
    [VELOCITY_ROTATE_KEY]: 45,
    [FACTOR_OF_SPEED_KEY]: 1
  })

  const adapter = new ChangeAngleVelosityAdaptor(obj)
  const command = new ChangeAngleVelosityCommand(adapter) 

  it('Объект изменил вектор мгновенной скорости скорость', () => {
    command.execute()
    command.execute()
    command.execute()

    const result = {
      position: { x: 10, y: 20 },
      velocity: { x: -1, y: 0 },
      'position-angle': 180,
      'velocity-rotate': 45,
      'factor-of-speed': 1
    } 

    expect(obj.attr).toEqual(result)
  })

})