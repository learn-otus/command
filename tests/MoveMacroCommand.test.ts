import { MacroCommand } from '../src/MacroCommands/MacroCommands';
import { Tank } from '../src/UObjects/Tank';
import {
  MovebleAdapter,
  POSITION_KEY,
  VELOCITY_KEY,
} from '../src/Adapters/MovebleAdapter';
import { CheckFuelCommand } from '../src/Commands/CheckFuelCommand';
import { BurnFuelCommand } from '../src/Commands/BurnFuelCommand';
import { MoveCommand } from '../src/Commands/MoveCommand';
import {
  FUEL_KEY,
  CONSUMPTION_FUEL_KEY,
  BurnebleFuelAdapter,
} from '../src/Adapters/BurnebleFuelAdapter';
describe('Проверка MoveMacroCommand', () => {
  
  it('Проверка на реализацию - CheckFuer BurnFuel Move - персонаж прошел', () => {
    const isMacroMovebleItem = new Tank({
      [POSITION_KEY]: { x: 10, y: 20 },
      [VELOCITY_KEY]: { x: 1, y: 3 },
      [FUEL_KEY]: 100,
      [CONSUMPTION_FUEL_KEY]: -5,
    });
  
    const moveAdapter = new MovebleAdapter(isMacroMovebleItem)
    const burnAdapter = new BurnebleFuelAdapter(isMacroMovebleItem)
    const checkAdapter = new BurnebleFuelAdapter(isMacroMovebleItem)
    const isMacroMovebleCommand = new MacroCommand([
      new CheckFuelCommand(checkAdapter),
      new BurnFuelCommand(burnAdapter),
      new MoveCommand(moveAdapter),
    ]);

    const result = {
      [POSITION_KEY]: {
        x: moveAdapter.getPosition().x + moveAdapter.getVelocity().x,
        y: moveAdapter.getPosition().y + moveAdapter.getVelocity().y
      },
      [VELOCITY_KEY]: {
        x: moveAdapter.getVelocity().x,
        y: moveAdapter.getVelocity().y
      },
      [FUEL_KEY]: burnAdapter.getFuel() + burnAdapter.getConsumptionFuel(),
      [CONSUMPTION_FUEL_KEY]: burnAdapter.getConsumptionFuel()
    }
    isMacroMovebleCommand.execute()
    expect(isMacroMovebleItem.attr).toEqual(result)
  });
  describe('Проверка на - exeption - при недостаточном fuel', () => {
    const isNoMacroMovebleItem = new Tank({
      [POSITION_KEY]: { x: 10, y: 20 },
      [VELOCITY_KEY]: { x: 1, y: 3 },
      [FUEL_KEY]: 3,
      [CONSUMPTION_FUEL_KEY]: -5,
    });

    const resultAttr = { ...isNoMacroMovebleItem.attr }

    const moveAdapter = new MovebleAdapter(isNoMacroMovebleItem)
    const burnAdapter = new BurnebleFuelAdapter(isNoMacroMovebleItem)
    const checkAdapter = new BurnebleFuelAdapter(isNoMacroMovebleItem)
    const isNoMacroMovebleCommand = new MacroCommand([
      new CheckFuelCommand(checkAdapter),
      new BurnFuelCommand(burnAdapter),
      new MoveCommand(moveAdapter),
    ]);
    it('Exeption выбрасываеться', () => {
      expect(() => isNoMacroMovebleCommand.execute()).toThrow()
    })
    it('Данные обьекта не поменялись', () => {
      expect(isNoMacroMovebleItem.attr).toEqual(resultAttr)
    })
  });
  })
  
