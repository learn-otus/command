import { MovebleAdapter, POSITION_KEY, VELOCITY_KEY } from '../src/Adapters/MovebleAdapter';
import { Tank } from '../src/UObjects/Tank';
import { MoveCommand } from '../src/Commands/MoveCommand';
describe('Тестируем команду MoveCommand', () => {
  const isMoveble = new MovebleAdapter(new Tank({[POSITION_KEY]: { x: 10, y: 20}, [VELOCITY_KEY]: { x: 1, y: 3}}))
  const isNoPosition = new MovebleAdapter(new Tank( {[VELOCITY_KEY]: { x: 1, y: 3}}))
  const isNoVelosity = new MovebleAdapter(new Tank( {[POSITION_KEY]: { x: 1, y: 3}}))

  const movbleCommand = new MoveCommand(isMoveble)
  const noPositionCommand = new  MoveCommand(isNoPosition)
  const noVelocityCommand = new  MoveCommand(isNoVelosity)

  test('MoveCommand передвижение объекта совершено', () => {
    const VALUE = {
      x: isMoveble.getPosition().x + isMoveble.getVelocity().x,
      y: isMoveble.getPosition().y + isMoveble.getVelocity().y
    }
    const test = {...isMoveble.getPosition()}
    movbleCommand.execute()
    expect(isMoveble.getPosition()).toEqual(VALUE)
  })

  test('MoveCommand ошибка если нет свойства "position"', () => {
    expect(() => noPositionCommand.execute()).toThrow()
  })

  test('MoveCommand ошибка если нет свойства "velocity"', () => {
    expect(() => noVelocityCommand.execute()).toThrow()
  })
})