import { ICommand } from '../../interfaces/ICommand';
import { scopes, defaultScopeValue } from '../scopes';
import { IoCCommand } from './IoCCommand';

// arg[0] - ScopeID: string
// arg[1] - Обьект скоупа
export class NewScopeRegister extends IoCCommand implements ICommand {
  execute() {
    if(!scopes[this.args[0]]) scopes[this.args[0]] = { ...defaultScopeValue , ...this.args[1] };
  }
}
