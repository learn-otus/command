import { ICommand } from '../../interfaces/ICommand';
import { scopes } from '../scopes';
import { IoCCommand } from './IoCCommand';
import { IoC } from '../IoC';
// arg[0] - ScopeID: string
export class CurrentScope extends IoCCommand implements ICommand {
  execute() {
    const currentScope = scopes[this.args[0]];
    if (currentScope) IoC.map = currentScope;
  }
}
