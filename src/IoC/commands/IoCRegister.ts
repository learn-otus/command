import { IoC } from '../IoC';
import { ICommand } from '../../interfaces/ICommand';
import { IoCCommand } from './IoCCommand';

export class IoCRegister extends IoCCommand implements ICommand {
  execute() {
    const key = this.args.shift()
    const callback = this.args.shift()
    IoC.map[key] = callback
  }
}