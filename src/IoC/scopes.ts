/*
  Скоуп сознательно организован в виде файла ts
  В последствии его можно настраивать через .env и т.д.
*/

import { IoCRegister } from './commands/IoCRegister';
import { NewScopeRegister } from './commands/NewScopeRegister';
import { CurrentScope } from './commands/CurrentScope';

export const defaultScopeValue = {
  'IoC.Register': (...args: any[]) => new IoCRegister(args),
  'Scopes.New': (...args: any[]) => new NewScopeRegister(args),
  'Scopes.Current': (...args: any[]) => new CurrentScope(args)
};

export const DEFAULT_ID = '1';

export const scopes: any = {
  '1': { ...defaultScopeValue }
};
