export type Vector = { x: number; y: number };

export interface IMovable {
  getPosition(): Vector;
  setPosition(vector: Vector): void;
  getVelocity(): Vector;
}
