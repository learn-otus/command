export interface IBurnebleFuel {
  getFuel(): number;
  setFuel(consumptionFuel: number): void;
  getConsumptionFuel(): number;
}
