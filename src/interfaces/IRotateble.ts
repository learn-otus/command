export interface IRotateble {
  getRotateAngle(): number;
  setRotateAngle(newPosition: number): void;
  getRotateVelosity(): number;
}
