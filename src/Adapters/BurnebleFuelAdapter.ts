import { UObjectAdapter } from './UObjectAdapter';
import { IBurnebleFuel } from '../interfaces/IBurnebleFuel';

export const FUEL_KEY = 'fuel';
export const CONSUMPTION_FUEL_KEY = 'consumption-fuel';

export class BurnebleFuelAdapter
  extends UObjectAdapter
  implements IBurnebleFuel
{
  getFuel(): number {
    const isKey = this.UObject.getProperty(FUEL_KEY);
    if(isKey === undefined) throw new Error(`CommandExeption : is not ${FUEL_KEY}:${isKey}`)
    return this.UObject.getProperty(FUEL_KEY);
  }

  setFuel(consumption: number): void {
    const valueIsKey = this.UObject.getProperty(FUEL_KEY);
    if(valueIsKey === undefined) throw new Error(`CommandExeption : is not ${FUEL_KEY}:${valueIsKey}`)
    this.UObject.setProperty(FUEL_KEY, valueIsKey + consumption);
  }

  getConsumptionFuel(): number {
    const isKey = this.UObject.getProperty(CONSUMPTION_FUEL_KEY);
    if(isKey === undefined) throw new Error(`CommandExeption : is not ${CONSUMPTION_FUEL_KEY}:${isKey}`)
    return this.UObject.getProperty(CONSUMPTION_FUEL_KEY);
  }
}
