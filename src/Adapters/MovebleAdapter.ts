import { IMovable, Vector } from '../interfaces/IMovable';
import { UObjectAdapter } from './UObjectAdapter';

export const POSITION_KEY = 'position'
export const VELOCITY_KEY = 'velocity'

export class MovebleAdapter extends UObjectAdapter implements IMovable {
  getPosition(): Vector {
    return this.UObject.getProperty(POSITION_KEY);
  }
  setPosition(value: Vector): void {
    this.UObject.setProperty(POSITION_KEY, value);
  }
  getVelocity(): Vector {
    return this.UObject.getProperty(VELOCITY_KEY);
  }
}
