import { IRotateble } from '../interfaces/IRotateble';
import { UObjectAdapter } from './UObjectAdapter';

export const POSITION_ANGLE_KEY = 'position-angle';
export const VELOCITY_ROTATE_KEY = 'velocity-rotate';

export class RotatebleAdapter extends UObjectAdapter implements IRotateble {
  getRotateAngle(): number {
    return this.UObject.getProperty(POSITION_ANGLE_KEY);
  }
  setRotateAngle(value: number) {
    this.UObject.setProperty(POSITION_ANGLE_KEY, value);
  }
  getRotateVelosity(): number {
    return this.UObject.getProperty(VELOCITY_ROTATE_KEY);
  }
}
