import { ICommand } from '../interfaces/ICommand';
import { BurnebleFuelAdapter } from '../Adapters/BurnebleFuelAdapter';

export class CheckFuelCommand implements ICommand {
  private adapter: BurnebleFuelAdapter;
  constructor(adapter: BurnebleFuelAdapter) {
    this.adapter = adapter;
  }
  execute() {
    check(this.adapter);
  }
}

function check(adapter: BurnebleFuelAdapter): void {
  if (adapter.getFuel() + adapter.getConsumptionFuel() <= 0)
    throw new Error(`CheckFuelCommand - No fuel`);
}
