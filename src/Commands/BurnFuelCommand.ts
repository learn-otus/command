import { ICommand } from '../interfaces/ICommand';
import { IBurnebleFuel } from '../interfaces/IBurnebleFuel';

export class BurnFuelCommand implements ICommand {
  private adapter: IBurnebleFuel;
  constructor(adapter: IBurnebleFuel) {
    this.adapter = adapter;
  }
  execute() {
    burn(this.adapter);
  }
}

function burn(adapter: IBurnebleFuel): void {
  adapter.setFuel(adapter.getConsumptionFuel())
}
