import { ICommand } from '../interfaces/ICommand';

export class MacroCommand implements ICommand {
  readonly commands: Array<ICommand>;

  constructor(commands: Array<ICommand>) {
    this.commands = commands;
  }

  execute() {
    this.commands.forEach((command) => {
      command.execute();
    });
  }
}
